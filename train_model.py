import numpy as np
from sklearn import datasets
from sklearn.linear_model import LogisticRegression
from joblib import dump

log_reg = LogisticRegression(multi_class="multinomial", C=10)
iris = datasets.load_iris()
X = iris["data"][:,(2,3)]
y = iris["target"]

log_reg.fit(X,y)
dump(log_reg, 'model.joblib')
print(log_reg.predict([[4,2]]))